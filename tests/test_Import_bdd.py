import unittest
import sqlite3 #Permet d'importer les fonction.
from fonction_import_bdd import *
import sys
import os

class Testimportbdd(unittest.TestCase):

    def setUp(self):
        self.connection = sqlite3.connect('test.sqlite3')
        self.cursor = self.connection.cursor()
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS auto(address_titulaire text,nom text,prenom text,immatriculation text,date_immatriculation date,vin text,marque text,denomination_commerciale text,couleur text,carrosserie text,categorie text,cylindre text,energie text,places text, poids text, puissance text,type text,variante text,version text )""")
        self.connection.commit()

    def tearDown(self):
        self.cursor.execute('''drop table auto''')
        self.connection.commit()
        self.connection.close()
        os.remove('test.sqlite3')


    def test_mise_bdd(self):
        
        sortie = {}
        sortie["address_titulaire"] = '982 Sanders Rapid Apt. 814 Jameschester, CO 06705'
        sortie["nom"] = 'Robles'
        sortie["prenom"] = 'Diana'
        sortie["immatriculation"] = '0-87577'
        sortie["date_immatriculation"] = '1935-05-30'
        sortie["vin"] = '9781238079037'
        sortie["marque"] = 'Miller-Brown'
        sortie["denomination_commerciale"] = 'Proactive impactful paradigm'
        sortie["couleur"] = 'Lavender'
        sortie["carrosserie"] = '88-8825014'
        sortie["categorie"] = '27-6194173'
        sortie["cylindre"] = '3344'
        sortie["energie"] = '16591493'
        sortie["places"] = '44'
        sortie["poids"] = '6056'
        sortie["puissance"] = '234'
        sortie["type"] = 'LLC'
        sortie["variante"]  = '62-3652072'
        sortie["version"] = '00275590'


        mise_bdd(sortie,self.cursor)

        self.cursor.execute("SELECT * FROM auto ")
    
        resultat = self.cursor.fetchall()
        self.assertEqual(len(resultat[0]),19)
        self.assertEqual(resultat[0],('982 Sanders Rapid Apt. 814 Jameschester, CO 06705','Robles','Diana','0-87577','1935-05-30','9781238079037','Miller-Brown','Proactive impactful paradigm','Lavender','88-8825014','27-6194173','3344','16591493','44','6056','234','LLC','62-3652072','00275590'))

    def test_update_ligne(self):

        sortie = {}
        sortie["address_titulaire"] = '982 Sanders Rapid Apt. 814 Jameschester, CO 06705'
        sortie["nom"] = 'Robles'
        sortie["prenom"] = 'Diana'
        sortie["immatriculation"] = '0-87577'
        sortie["date_immatriculation"] = ' '
        sortie["vin"] = '9781238079037'
        sortie["marque"] = 'Miller-Brown'
        sortie["denomination_commerciale"] = 'Proactive impactful paradigm'
        sortie["couleur"] = 'Lavender'
        sortie["carrosserie"] = '88-8825014'
        sortie["categorie"] = '27-6194173'
        sortie["cylindre"] = '3344'
        sortie["energie"] = '16591493'
        sortie["places"] = '44'
        sortie["poids"] = '6056'
        sortie["puissance"] = '234'
        sortie["type"] = 'LLC'
        sortie["variante"]  = ' '
        sortie["version"] = '00275590'

        self.cursor.execute("""INSERT INTO auto(address_titulaire,nom,prenom,immatriculation,date_immatriculation,vin,marque,denomination_commerciale,couleur,carrosserie,categorie,cylindre,energie,places,poids,puissance,type,variante,version) VALUES(:address_titulaire,:nom,:prenom,:immatriculation,:date_immatriculation,:vin,:marque,:denomination_commerciale,:couleur,:carrosserie,:categorie,:cylindre,:energie,:places,:poids,:puissance,:type,:variante,:version)""",sortie)

        sortie_update = {}
        sortie_update["address_titulaire"] = '982 Sanders Rapid Apt. 814 Jameschester, CO 06705'
        sortie_update["nom"] = 'Louis'
        sortie_update["prenom"] = 'Diana'
        sortie_update["immatriculation"] = '0-87577'
        sortie_update["date_immatriculation"] = '1935-05-30'
        sortie_update["vin"] = '9781238079037'
        sortie_update["marque"] = 'Miller-Brown'
        sortie_update["denomination_commerciale"] = 'Proactive impactful paradigm'
        sortie_update["couleur"] = 'Lavender'
        sortie_update["carrosserie"] = '88-8825014'
        sortie_update["categorie"] = '27-6194173'
        sortie_update["cylindre"] = '3344'
        sortie_update["energie"] = '16591493'
        sortie_update["places"] = '44'
        sortie_update["poids"] = '6056'
        sortie_update["puissance"] = '234'
        sortie_update["type"] = 'LLC'
        sortie_update["variante"]  = '62-3652072'
        sortie_update["version"] = '00275590'

        update_ligne(sortie_update,self.cursor)

        self.cursor.execute("SELECT * FROM auto ")
    
        resultat = self.cursor.fetchall()
        self.assertEqual(len(resultat[0]),19)
        self.assertEqual(resultat[0],('982 Sanders Rapid Apt. 814 Jameschester, CO 06705','Louis','Diana','0-87577','1935-05-30','9781238079037','Miller-Brown','Proactive impactful paradigm','Lavender','88-8825014','27-6194173','3344','16591493','44','6056','234','LLC','62-3652072','00275590'))



   
        
