import os
import csv
import sqlite3
import logging
import sys
import argparse

#Fonction permettant de vérifier les arguments passés en paramètre du programme.
def saisie() :
    parser = argparse.ArgumentParser()
    parser.add_argument("nom_CSV", help="Donner le nom du CSV", type=str)
    parser.add_argument("nom_de_la_bdd", help="Donner le nom de la bdd", type=str)
    args = parser.parse_args()
   
    while (os.path.exists(args.nom_CSV) == False | os.path.isfile(args.nom_CSV) != True) :

        logging.error("Fichier CSV introuvable")
        #sys.exit("Fichier CSV introuvable")

    return args.nom_CSV,args.nom_de_la_bdd
       


#Permet de se connecter à la bdd et de la créée si elle n'existe pas
def creation_bdd(saisiebdd) :

    if os.path.isfile(saisiebdd) :
        logging.info("Fichier de la bdd trouvé")
        base_de_donnee = sqlite3.connect(saisiebdd)
    else :
        logging.info("Fichier de la bdd non trouvé création de cette dernière")
        base_de_donnee = sqlite3.connect(saisiebdd)
        base_de_donnee.execute("""CREATE TABLE IF NOT EXISTS auto(address_titulaire text,nom text,prenom text,immatriculation text,date_immatriculation date,vin text,marque text,denomination_commerciale text,couleur text,carrosserie text,categorie text,cylindre text,energie text,places text, poids text, puissance text,type text,variante text,version text )""")
    
    return base_de_donnee

#Vérification si la ligne existe si non création de cette dernière
def mise_bdd(ligne,cursor) :
    
    cursor.execute("SELECT * FROM auto WHERE immatriculation = :immatriculation",ligne)
    
    ligne1 = cursor.fetchall()
    
    if (len( ligne1 ) == 0):
        cursor.execute("""INSERT INTO auto(address_titulaire,nom,prenom,immatriculation,date_immatriculation,vin,marque,denomination_commerciale,couleur,carrosserie,categorie,cylindre,energie,places,poids,puissance,type,variante,version) VALUES(:address_titulaire,:nom,:prenom,:immatriculation,:date_immatriculation,:vin,:marque,:denomination_commerciale,:couleur,:carrosserie,:categorie,:cylindre,:energie,:places,:poids,:puissance,:type,:variante,:version)""",ligne)
        logging.info("Ajout d'une entrée")
    else:
        logging.debug(ligne1[0])
        update_ligne(ligne,cursor)

#Modification de la ligne si incomplète ou mauvaise
def update_ligne(ligne,c) :
    c.execute("""UPDATE auto SET address_titulaire = :address_titulaire, nom = :nom,prenom = :prenom,date_immatriculation = :date_immatriculation,vin = :vin,marque = :marque,denomination_commerciale = :denomination_commerciale,couleur = :couleur,carrosserie = :carrosserie,categorie = :categorie,cylindre = :cylindre,energie = :energie,places = :places,poids = :poids,puissance = :puissance,variante = :variante ,version = :version WHERE immatriculation = :immatriculation""",ligne)
    logging.info("Complétion de ligne")
