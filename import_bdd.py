import os
import csv
import sqlite3
import logging
import sys
import argparse
from fonction_import_bdd import *


#Mise en place du fichier log
logging.basicConfig(filename='Log_import_bdd', level=logging.DEBUG)

saisiecsv,saisiebdd = saisie()


with open(saisiecsv) as cvsfile: #Ouverture en mode lecture du fichier à traiter
    logging.info("Ouverture du fichier")
    reader = csv.DictReader(cvsfile,delimiter='!') #Lecture du fichier

    base_de_donnee = creation_bdd(saisiebdd)
    

    c = base_de_donnee.cursor()

    for sortie in reader :
        mise_bdd(sortie,c)
        

    logging.info("Fin du programme")
    base_de_donnee.commit()